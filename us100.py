from machine import UART, Pin
import utime

class US100:

    def __init__(self, tpin=11, rpin=12):
        self.tx_pin = tpin
        self.rx_pin = rpin
        self.uart = UART(2, baudrate=9600, tx=Pin(self.tx_pin), rx=Pin(self.rx_pin))

    def _send_command(self, command):
        self.uart.write(command)
        utime.sleep_ms(500)
    def _read_response(self):
        return self.uart.read(2)

    def measure_distance(self):
        try:
            self._send_command(b'\x55')
            response = self._read_response()
            print(response and len(response))
            print(response)
            if response and len(response) == 2:
                distance = (response[0] << 8) | response[1]
                print(distance)
                if distance > 0 and distance <= 4500:  # Convert to millimeters
                    return distance
            return -1  # Invalid or out-of-range reading
        except Exception as exc:
            print("Error:", exc)
            return -1
