from machine import Pin, ADC, I2C, SPI, UART
import mq135
import utime
import math
from us100 import US100
import st7789

mq = mq135.MQ135(Pin(13))

while True:
#温湿度
    utime.sleep_ms(40)

    i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000)

    i2cAddress = i2c.scan()

    i2c.write(i2cAddress[0], bytes(0x71))

    data = i2c.read(i2cAddress[0], 1)

    def displayBinaryString(bArray):
        binStr = ''
        for b in bArray:
            oneStr = ''

            tmp = b
            while(tmp != 0):
                oneStr = oneStr + str(tmp & 0x01)
                tmp = (tmp >> 1)

            if(len(oneStr) != 8):
                oneStr = oneStr + '0' * (8 - len(oneStr) % 8)
            
            binStr = binStr + ' ' + ''.join(reversed(oneStr))

        return binStr

    statusBit = (data[0] >> 3) & 1

    if(statusBit != 0x01):
        i2c.write(i2cAddress[0], bytes([0xBE, 0x08, 0x00]))

    i2c.write(i2cAddress[0], bytes([0xAC, 0x33, 0x00]))

    utime.sleep_ms(75)
    sensorData = i2c.read(i2cAddress[0], 6)

    while((sensorData[0] >> 7) & 0x01 != 0):
        utime.sleep_ms(5)
        sensorData = i2c.read(i2cAddress[0], 6)

    humidity = (sensorData[1] << 12) | (sensorData[2] << 4) | (sensorData[3] >> 4)
    humidity = humidity * 100 / 1048576

    temperature = ((sensorData[3] & 0x0F) << 16) | (sensorData[4] << 8) | sensorData[5]
    temperature = temperature * 200 / 1048576 - 50
    print(humidity)
    print(temperature)
    #温湿度
    #臭味

    rzero = mq.get_rzero()

    corrected_rzero = mq.get_corrected_rzero(temperature, humidity)

    resistance = mq.get_resistance()

    ppm = mq.get_ppm()
    corrected_ppm = mq.get_corrected_ppm(temperature, humidity)

    adc = ADC(Pin(13))

    value = adc.read()
    ppm1 = corrected_ppm/100000
    print("%ppm:" + str(ppm1) + "%")
    utime.sleep(0.3)
    #臭味

    #距离
    us100_sensor = US100()

    distance = us100_sensor.measure_distance()
    if distance != -1:
        print("Distance:", distance, "mm")
    else:
        print("Error reading distance")
    utime.sleep_ms(500)
    #距离
    G02 = Pin(2)
    G05 = Pin(5)
    G02.init(Pin.OUT)
    G05.init(Pin.OUT)
    if(ppm1>5):
        G02.on()
        i=1
        while(i<20):
            i+=1
            distance = us100_sensor.measure_distance()
            if distance != -1:
                print("Distance:", distance, "mm")
            else:
                print("Error reading distance")
            utime.sleep_ms(500)
            if(distance>300):
                G05.on()
            else:
                G05.off()
            utime.sleep(1)
    G02.off()
    G05.off()
