import math
from machine import ADC, Pin
import utime



class MQ135():
    """ Class for dealing with MQ13 Gas Sensors """
    # The load resistance on the board
    RLOAD = 10.0
    # Calibration resistance at atmospheric gas level
    RZERO = 76.63
    # Parameters for calculating ppm of gas from sensor resistance
    PARA = 116.6020682
    PARB = 2.769034857

    # Parameters to model temperature and humidity dependence
    CORA = 0.00035
    CORB = 0.02718
    CORC = 1.39538
    CORD = 0.0018
    CORE = -0.003333333
    CORF = -0.001923077
    CORG = 1.130128205
    ATMOgas = 397.13

    def __init__(self, pin):
        self.pin = pin
        
    def a(self):
        return 1

    def get_correction_factor(self, temperature, humidity):
        if temperature < 20:
            return self.CORA * temperature * temperature - self.CORB * temperature + self.CORC - (humidity - 33.) * self.CORD

        return self.CORE * temperature + self.CORF * humidity + self.CORG

    def get_resistance(self):
        """Returns the resistance of the sensor in kOhms // -1 if not value got in pin"""
        adc = ADC(self.pin)
        value = adc.read()
        if value == 0:
            return -1

        return (1023./value - 1.) * self.RLOAD

    def get_corrected_resistance(self, temperature, humidity):
        #Gets the resistance of the sensor corrected for temperature/humidity
        return self.get_resistance()/ self.get_correction_factor(temperature, humidity)

    def get_ppm(self):
        """Returns the ppm of gas sensed """
        return self.PARA * pow((self.get_resistance()/ self.RZERO), -self.PARB)

    def get_corrected_ppm(self, temperature, humidity):
        """Returns the ppm of  gas sensed 
        corrected for temperature/humidity"""
        return self.PARA * pow((self.get_corrected_resistance(temperature, humidity)/ self.RZERO), -self.PARB)

    def get_rzero(self):
        data = self.get_resistance() * pow((self.ATMOgas/self.PARA), (1./self.PARB))
        return data
        """Returns the resistance RZero of the sensor (in kOhms) for calibratioin purposes"""
    def get_corrected_rzero(self, temperature, humidity):
        """Returns the resistance RZero of the sensor (in kOhms) for calibration purposes
        corrected for temperature/humidity"""
        return self.get_corrected_resistance(temperature, humidity) * pow((self.ATMOgas/self.PARA), (1./self.PARB))
